<?php

namespace App;
use Vinelab\NeoEloquent\Eloquent\Model as NeoEloquent;

class Writer extends NeoEloquent {
  protected $label = 'writers';
   protected $guarded = [];

  public function books()
    {
        return $this->hasMany('App\Book', 'WROTE');
    }
}

<?php

namespace App;
use Vinelab\NeoEloquent\Eloquent\Model as NeoEloquent;
class Publisher extends NeoEloquent {
  protected $label = 'publishers';
  protected $guarded = [];

  public function book()
    {
        return $this->hasMany('Book', 'PUBLISHED');
    }

}

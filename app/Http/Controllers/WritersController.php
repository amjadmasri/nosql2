<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Writer;
use App\Book;
use Illuminate\Support\Facades\Storage;



class WritersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $writers = Writer::All();
        return view('admin.authors.index', ['authors' => $writers]);
    }

    public function browse()
    {
        $writers = Writer::All();
        return view('user.authors', ['authors' => $writers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.authors.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $data = $request->input();
      unset($data['photo']);
      if(request()->hasFile('photo') && request()->file('photo')->isValid())
      {
          $path = Storage::putFile('authors',request()->file('photo'));
          $data['photo'] = '/storage/'.$path;
      }
      $writer = Writer::create($data);
      return redirect('/cms/authors');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $writer = Writer::with('books.writers')->find($id);
        return view('user.author', ['author' => $writer]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $writer = Writer::find($id);
        return view('admin.authors.edit', ['author' => $writer]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $data = $request->input();
      unset($data['photo']);
      if(request()->hasFile('photo') && request()->file('photo')->isValid())
      {
          $path = Storage::putFile('authors',request()->file('photo'));
          $data['photo'] = '/storage/'.$path;
      }
      $writer = Writer::find($id);
      $writer->fill($data);
      $writer->save();
      return view('admin.authors.edit', ['author' => $writer]);

    }

    public function addBooks(Request $request,$id){
        //dd($request->all());
        $writer=Writer::find($id);


        foreach($request->all() as $req){
            $book = Book::create($req);
            $posted = $writer->books()->save($book);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

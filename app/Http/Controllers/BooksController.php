<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\Character;
use App\Writer;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;


class BooksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::All();
        return view('admin.books.index', ['books' => $books]);

    }

    public function browse()
    {
      $books = Book::with('writers', 'reviewers')->get();
      return view('user.books', ['books' => $books]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $writers = Writer::all();
        return view('admin.books.create', ['authors' => $writers]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $data = $request->input();
      unset($data['cover']);
      $authors = $request->input('authors');
      unset($data['authors']);
      if(request()->hasFile('cover') && request()->file('cover')->isValid())
      {
          $path = Storage::putFile('books',request()->file('cover'));
          $data['cover'] = '/storage/'.$path;
      }
      $book = Book::create($data);
      $authors = Writer::find($authors);
      foreach ($authors as $author) {
        $author->books()->save($book);
      }
      return redirect('/cms/books');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $book = Book::with('writers')->find($id);
        $reviews = $book->reviewers()->edges();
        return view('user.book',  ['book' => $book, 'reviews' => $reviews]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $book = Book::find($id);
        $writers = Writer::all();
        return view('admin.books.edit', ['book' => $book, 'authors' => $writers]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->input();
        unset($data['cover']);
        if(request()->hasFile('cover') && request()->file('cover')->isValid())
        {
            $path = Storage::putFile('books',request()->file('cover'));
            $data['cover'] = '/storage/'.$path;
        }
        $book = Book::find($id);
        $book->fill($data);
        $book->save();
        return view('admin.books.edit', ['book' => $book]);
    }

    public function addCharacters(Request $request,$id){
        //dd($request->all());
        $book=Book::find($id);


        foreach($request->all() as $req){
            $character = Character::create($req);
            $posted = $book->characters()->save($character);
        }
    }

    public function review(Request $request, $id)
    {
      $book = Book::with('writers')->find($id);
      $user = Auth::user();
      $review = $book->reviewers()->save($user);
      $review->rating = $request->input('rating');
      $review->text = $request->input('text');
      $review->save();
      return redirect('/cms/books/' . $book->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App;
use Vinelab\NeoEloquent\Eloquent\Model as NeoEloquent;

class Character extends NeoEloquent {
  protected $label = 'characters';

  protected $guarded = [];

  public function books()
    {
        return $this->belongsToMany('Book', 'APPEARED_IN');
    }

}

<?php

namespace App;
use Vinelab\NeoEloquent\Eloquent\Model as NeoEloquent;
use App\Character;

class Book extends NeoEloquent {
  protected $label = 'books';
  protected $guarded = [];

  public function writers()
  {
      return $this->belongsToMany('App\Writer', 'WROTE');
  }

  public function characters()
  {
      return $this->hasMany(Character::class, 'HAS_CHARACTER');
  }

  public function publishers()
  {
      return $this->belongsToMany('Publisher', 'PUBLISHED_BY');
  }

  public function reviewers()
  {
    return $this->belongsToMany('App\User', 'REVIEWED');
  }




}

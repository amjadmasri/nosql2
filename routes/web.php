<?php

use App\Writer;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth'])->group(function() {

  Route::put('users/followUser/{id}','UsersController@followUser');
  Route::resource('users', 'UsersController');

  Route::get('/books', 'BooksController@browse');

  Route::get('/books/{id}', 'BooksController@show');

  Route::post('/books/{id}/review', 'BooksController@review');

  Route::get('/authors', 'WritersController@browse');

  Route::get('/authors/{id}', 'WritersController@show');

});

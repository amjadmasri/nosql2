<?php

Route::put('books/addCharacters/{id}','BooksController@addCharacters');
Route::resource('books', 'BooksController');

Route::put('authors/addBooks/{id}','WritersController@addBooks');
Route::resource('authors', 'WritersController');

Route::put('publishers/addBooks/{id}','PublishersController@addBooks');
Route::resource('publishers', 'PublishersController');

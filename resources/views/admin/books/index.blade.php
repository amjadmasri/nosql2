@extends('layouts.admin')

@section('side')
  <h2>Books</h2>
  <a class="btn btn-info d-block" href="/cms/books/create"><i class="fa fa-plus fa-fw"></i> Add Book</a>
@endsection

@section('content')
  <div class="row">
    @foreach ($books as $book)
      <div class="col-6 mb-5">
        <div class="media">
          <img class="mr-3" src="{{$book->cover}}" alt="Book Cover" style="width: 5rem;">
          <div class="media-body">
            <h5>{{$book->title}}</h5>
            <p>Published: {{$book->published}}<br>
              Synposis: {{$book->desc}}
            </p>
            <div>
              <a href="/cms/books/{{$book->id}}/edit" class="card-link">Edit</a>
            </div>
          </div>
        </div>
      </div>
    @endforeach
  </div>
@endsection

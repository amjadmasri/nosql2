@extends('layouts.admin')

@section('side')
  <h2>Add Book</h2>
  <a class="btn btn-link" href="/cms/books"><i class="fa fa-arrow-left fa-fw"></i> Back</a>
@endsection

@section('content')
  <form action="/cms/books" method="POST" enctype="multipart/form-data">
    <div class="form-group">
      <label>Title</label>
      <input type="text" class="form-control" name="title">
    </div>
    <div class="form-group">
      <label>Synposis</label>
      <textarea class="form-control" name="desc" row="3"></textarea>
    </div>
    <div class="form-group">
      <label>Genre</label>
      <input type="text" class="form-control" name="genre">
    </div>
    <div class="form-group">
      <label>Year of Publishing</label>
      <input type="text" class="form-control" name="published">
    </div>
    <div class="form-group">
      <label>Authors</label>
      <select multiple class="custom-select" name="authors[]">
        @foreach ($authors as $author)
          <option value="{{$author->id}}">{{$author->name . ' (' . $author->nation . ')'}}</option>
        @endforeach
      </select>
    </div>
    <div class="form-group">
      <label>Cover</label>
      <input type="file" class="form-control-file" name="cover">
    </div>
    <button type="submit" class="btn btn-dark">Submit</button>
</form>
@endsection

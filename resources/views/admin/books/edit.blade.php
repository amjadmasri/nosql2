@extends('layouts.admin')

@section('side')
  <h2>Add Book</h2>
  <a class="btn btn-link" href="/cms/books"><i class="fa fa-arrow-left fa-fw"></i> Back</a>
@endsection

@section('content')
  <div class="media">
    <img class="mr-3" src="{{$book->cover}}" alt="Book Cover" style="width: 10rem;">
    <div class="media-body">
      <form action="/cms/books/{{$book->id}}" method="POST" enctype="multipart/form-data">
        {{ method_field('PUT') }}
        <div class="form-group">
          <label>Title</label>
          <input type="text" class="form-control" name="title" value="{{$book->title}}">
        </div>
        <div class="form-group">
          <label>Synposis</label>
          <textarea class="form-control" name="desc" row="3">{{$book->desc}}</textarea>
        </div>
        <div class="form-group">
          <label>Genre</label>
          <input type="text" class="form-control" name="genre" value="{{$book->genre}}">
        </div>
        <div class="form-group">
          <label>Year of Publishing</label>
          <input type="text" class="form-control" name="published" value="{{$book->published}}">
        </div>
        <div class="form-group">
          <label>Cover</label>
          <input type="file" class="form-control-file" name="cover">
        </div>
        <button type="submit" class="btn btn-dark">Submit</button>
      </form>
    </div>
  </div>
@endsection

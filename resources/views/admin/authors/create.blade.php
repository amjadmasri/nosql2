@extends('layouts.admin')

@section('side')
  <h2>Add Author</h2>
  <a class="btn btn-link" href="/cms/authors"><i class="fa fa-arrow-left fa-fw"></i> Back</a>
@endsection

@section('content')
  <form action="/cms/authors" method="POST" enctype="multipart/form-data">
    <div class="form-group">
      <label>Name</label>
      <input type="text" class="form-control" name="name">
    </div>
    <div class="form-group">
      <label>Bio</label>
      <textarea class="form-control" name="bio" row="3"></textarea>
    </div>
    <div class="form-group">
      <label>Nationality</label>
      <input type="text" class="form-control" name="nation">
    </div>
    <div class="form-group">
      <label>Born</label>
      <input type="text" class="form-control" name="born">
    </div>
    <div class="form-group">
      <label>Died</label>
      <input type="text" class="form-control" name="died">
    </div>
    <div class="form-group">
      <label>Photo</label>
      <input type="file" class="form-control-file" name="photo">
    </div>
    <button type="submit" class="btn btn-dark">Submit</button>
</form>
@endsection

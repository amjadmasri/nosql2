@extends('layouts.admin')

@section('side')
  <h2>Add Author</h2>
  <a class="btn btn-link" href="/cms/authors"><i class="fa fa-arrow-left fa-fw"></i> Back</a>
@endsection

@section('content')
  <div class="media">
    <img class="mr-3" src="{{$author->photo}}" alt="author Cover" style="width: 10rem;">
    <div class="media-body">
      <form action="/cms/authors/{{$author->id}}" method="POST" enctype="multipart/form-data">
        {{ method_field('PUT') }}
        <div class="form-group">
          <label>Name</label>
          <input type="text" class="form-control" name="name" value="{{$author->name}}">
        </div>
        <div class="form-group">
          <label>Bio</label>
          <textarea class="form-control" name="bio" row="3">{{$author->bio}}</textarea>
        </div>
        <div class="form-group">
          <label>Nationality</label>
          <input type="text" class="form-control" name="nation" value="{{$author->nation}}">
        </div>
        <div class="form-group">
          <label>Born</label>
          <input type="text" class="form-control" name="born" value="{{$author->born}}">
        </div>
        <div class="form-group">
          <label>Died</label>
          <input type="text" class="form-control" name="died" value="{{$author->died}}">
        </div>
        <div class="form-group">
          <label>Photo</label>
          <input type="file" class="form-control-file" name="photo">
        </div>
        <button type="submit" class="btn btn-dark">Submit</button>
      </form>
    </div>
  </div>
@endsection

@extends('layouts.admin')

@section('side')
  <h2>Authors</h2>
  <a class="btn btn-info d-block" href="/cms/authors/create"><i class="fa fa-plus fa-fw"></i> Add Author</a>
@endsection

@section('content')
  <div class="row">
    @foreach ($authors as $author)
      <div class="col-6 mb-5">
        <div class="media">
          <img class="mr-3" src="{{$author->photo}}" alt="Author Photo" style="width: 5rem;">
          <div class="media-body">
            <h5>{{$author->name}}</h5>
            <p>Nationality: {{$author->nation}}<br>
              Bio: {{$author->bio}}
            </p>
            <div>
              <a href="/cms/authors/{{$author->id}}/edit" class="card-link">Edit</a>
            </div>
          </div>
        </div>
      </div>
    @endforeach
  </div>
@endsection

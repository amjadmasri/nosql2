@extends('layouts.master')

@include('admin.components.navbar')

@section('page')
  <div class="container mt-5 mb-5">
    <div class="row">
      <div class="col-3">
        @yield('side')
      </div>
      <div class="col-9 border-left border-secondary">
        @yield('content')
      </div>
    </div>
  </div>
@endsection

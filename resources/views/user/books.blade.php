@extends('layouts.user')

@section('content')
  <div class="row">
    @foreach ($books as $book)
      <div class="col-6 mb-5">
        <div class="media">
          <img class="mr-3" src="{{$book->cover}}" alt="Book Cover" style="width: 5rem;">
          <div class="media-body">
            <h5>{{$book->title}}</h5>
            <p>Published: {{$book->published}}<br>
               Genre: {{$book->genre}}<br>
               Synposis: {{$book->desc}}<br>
               Authors:
               @foreach ($book->writers as $key => $author)
                 <a href="/authors/{{$author->id}}">{{$author->name}}</a>
                 @if ($key != count($book->writers) - 1)
                   ,
                 @endif
               @endforeach
            </p>
            <div>
              <a href="/books/{{$book->id}}"><small>Details</small></a>
            </div>
          </div>
        </div>
      </div>
    @endforeach
  </div>
@endsection

@extends('layouts.user')

@section('content')
  <div class="row">
    @foreach ($authors as $author)
      <div class="col-6 mb-5">
        <div class="media">
          <img class="mr-3" src="{{$author->photo}}" alt="Author Photo" style="width: 5rem;">
          <div class="media-body">
            <h5>{{$author->name}}</h5>
            <p>Nationality: {{$author->nation}}<br>
              Bio: {{$author->bio}}
            </p>
            <div>
              <a href="/authors/{{$author->id}}" class="card-link"><small>View Books</small></a>
            </div>
          </div>
        </div>
      </div>
    @endforeach
  </div>
@endsection

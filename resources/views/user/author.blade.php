@extends('layouts.user')

@section('content')
  <div class="row">
    <div class="col-12">
      <div class="media">
        <img class="mr-3" src="{{$author->photo}}" alt="Author Photo" style="width: 10rem;">
        <div class="media-body">
          <h5>{{$author->name}}</h5>
          <p>Nationality: {{$author->nation}}<br>
            Bio: {{$author->bio}}
          </p>
        </div>
      </div>
    </div>
  </div>
  <hr>
  <h3>Books by {{$author->name}} <small>({{count($author->books)}})</small></h3>
  <div class="row mt-5">
    @foreach ($author->books as $book)
      <div class="col-6 mb-5">
        <div class="media">
          <img class="mr-3" src="{{$book->cover}}" alt="Book Cover" style="width: 5rem;">
          <div class="media-body">
            <h5>{{$book->title}}</h5>
            <p>Published: {{$book->published}}<br>
               Genre: {{$book->genre}}<br>
               Synposis: {{$book->desc}}<br>
               Collaborators:
               @foreach ($book->writers as $key => $w)
                 @continue($w->id == $author->id)
                 <a href="/authors/{{$w->id}}">{{$w->name}}</a>
                 @if ($key != count($book->writers) - 1)
                   ,
                 @endif
               @endforeach
            </p>
          </div>
        </div>
      </div>
    @endforeach
  </div>
@endsection

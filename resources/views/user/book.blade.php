@extends('layouts.user')

@section('content')
  @php
    $own_review = null;
    $rating = 0;
    foreach ($reviews as $review)
    {
      if ($review->related()->id == Auth::id())
      {
        $own_review = $review;
      }
      $rating += $review->rating;
    }
    if ($rating != 0)
    {
      $rating /= count($reviews);
    }
  @endphp
  <div class="row">
    <div class="col-12">
      <div class="media">
        <img class="mr-3" src="{{$book->cover}}" alt="Book Cover" style="width: 10rem;">
        <div class="media-body">
          <h5>{{$book->title}}</h5>
          <p>Published: {{$book->published}}<br>
             Genre: {{$book->genre}}<br>
             Synposis: {{$book->desc}}<br>
             Authors:
             @foreach ($book->writers as $key => $author)
               <a href="/authors/{{$author->id}}">{{$author->name}}</a>
               @if ($key != count($book->writers) - 1)
                 ,
               @endif
             @endforeach
          </p>
          <div class="row">
            <div class="col-4">
              <small>Rating: {{$rating}}</small>
              <div class="progress" style="height: 3px">
                <div class="progress-bar bg-success" role="progressbar" style="width: {{($rating * 10) . '%'}};"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <hr>
  <h3>Reviews <small></small></h3>
  <div class="row mt-3">
    <div class="col-12">
      <div class="media">
        <img class="rounded-circle mr-3" src="/assets/user.png" alt="Book Cover" style="width: 3rem;">
        <div class="media-body">
          <h5>You @if($own_review)<small>Rating: {{$own_review->rating}}</small>@endif</h5>
          @if ($own_review)
            <p>{{$own_review->text}}</p>
          @else
            <form action="/books/{{$book->id}}/review" method="POST">
              <div class="form-group">
                <label>Rating</label>
                <select class="form-control w-25" name="rating">
                  @for ($i = 1 ; $i <= 10 ; $i++)
                    <option value="{{$i}}">{{$i}}</option>
                  @endfor
                </select>
              </div>
              <div class="form-group">
                <label>Review</label>
                <textarea class="form-control w-75" name="text" row="7" placeholder="Type here ..."></textarea>
              </div>
              <button type="submit" class="btn btn-info">Submit</button>
            </form>
          @endif
        </div>
      </div>
    </div>
  </div>
  <hr>
  @foreach ($reviews as $review)
    @continue($review->related()->id == Auth::id())
    <div class="row">
      <div class="col-12 mt-3">
        <div class="media">
          <img class="rounded-circle mr-3" src="/assets/user.png" alt="Book Cover" style="width: 3rem;">
          <div class="media-body">
            <h5>{{$review->related()->name}} <small>Rating: {{$review->rating}}</small></h5>
            <p>{{$review->text}}</p>
          </div>
        </div>
      </div>
    </div>
  @endforeach
@endsection
